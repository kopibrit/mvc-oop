<?php
namespace Setting;
use \PDO;

class Crud extends Koneksi {
  public $kondisi;
  public $query;
  Protected $setbindparam=[];
  Protected $setbindwhere=[];

  public function select($data) {
    $this->query = "SELECT " . $data;
    $this->kondisi = "select";
    return $this;
  }
  public function from($table) {
    $this->query .= "FROM " . $table;
    return $this;
  }
  public function where($str=array()) {
    $this->query .= "WHERE ";
    $loop = 0 ;
    foreach ($str as $keystr) {
      foreach ($keystr as $key => $value) {
        $this->query .= $key . " ? ";
        $array_push ($this->$setbindwhere, $value);
        if ($loop<count ($str)-1) {
          $this->query .= " AND ";
        }
        $loop++;
      }
    }
    return $this;
  }
  Protected function kondisiselect() {
    $stmt = $this->db->prepare($this->query);
    $loop = 1;
    foreach ($this->$setbindwhere as $value) {
      $stmt->bindValue($loop++,$value);
    }
    $stmt->execute();
    $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $row;
  }
  public function execute() {
    if ($this->kondisi=='select') {
      return $this->kondisiselect();
    }
  }
}
?>
