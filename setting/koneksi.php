<?php
namespace Setting;
use \PDO;

class Koneksi {

  Private $dbhost = "localhost";
  Private $dbuser = "root";
  Private $dbpass = "";
  Private $dbname = "db_appskpu";
  Protected $db;

  public function __construct() {
    if (!$this->db) {
      try {
        $konek = new PDO ("mysql:host=$this->dbhost;dbname=$this->dbname", $this->dbuser, $this->dbpass);
        $konek->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      }catch (PDOException $e) {
        echo "Koneksi Gagal" . $e->getMessage();
      }
      $this->db = $konek;
    }
  }
}
?>
