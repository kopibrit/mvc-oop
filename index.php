 <!DOCTYPE html>
 <html lang="en">

<head>
     <meta charset="UTF-8">
     <title>TITLE</title>
     <meta name="description" content="DESCRIPTION">
    <link rel="stylesheet" href="PATH">

	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
     <!--[if lt IE 9]>
       <script src = "http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
     <![endif]-->
 </head>

 <body>
   <div class="container">
     <div id="page-wrapper">
       <div class="row">
          <div class="col-lg-12">
            <h2 class="page-header">Tampil User</h2>
          </div>
       </div>
       <buttton type="button" class="btn btn-default" data-toggle="modal" data-target="#tambahmodal">Tambah</buttton>
       <div id="showmodaledit"></div>
       <div id="tambahmodal" class="modal fade" role="dialog">
         <div class="modal-dialog">
         <div class="modal-content">
         <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></button>
          <h4 class="modal-title">Tambah Data</h4>
         </div>
         <div class="modal-body">
           <?php include_once("tambah.php") ?>
         </div>
         </div>
         </div>
       </div>
       <div id="ajaxread"></div>
     </div>
   </div>
   <script
 			  src="https://code.jquery.com/jquery-1.9.1.min.js"
 			  integrity="sha256-wS9gmOZBqsqWxgIVgA8Y9WcQOa7PgSIX+rPA0VL2rbQ="
 			  crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <script src="data.js"></script>
 </body>
 </html>
