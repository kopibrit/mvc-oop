<?php
error_reporting(0);
require_once("setting/koneksi.php");
require_once("setting/crud.php");

use Setting\Koneksi;
use Setting\Crud;

$db = new Crud();

if (isset($_GET['halaman'])) {
  $alldata = $db->select('*')->from ('users')->execute();
?>
<table class="table table-striped table-hover">
  <thead>
    <tr>
      <th>No.</th>
      <th>Nama Lengkap</th>
      <th>Email</th>
      <th>Status</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <?php $no=1;
    foreach ($alldata as $value):
    ?>
    <tr>
      <td><?=$no++;?></td>
      <td><?=$value['nama_lengkap'];?></td>
      <td><?=$value['email'];?></td>
      <td><?=$value['blokir'];?></td>
      <td><a href="javascript:void(0)" class="btn btn-default" onclick="tampiledit('<?=$value['id_session'];?>')">edit</a></td>
      <td><a href="javascript:void(0)" class="btn btn-default" onclick="deletedata('<?=$value['id_session'];?>')">hapus</a></td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>
<?php }
?>
